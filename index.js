const express = require("express");
const { Pool } = require("pg");

const app = express();
const port = 4000;

const pool = new Pool({
    user: "postgres",
    host: "localhost",
    database: "todos_db",
    password: "postgres",
    port: "54322",
  });

// Modelo
class Model{
    async getTodos(){
        const { rows } = await pool.query("select * from todos;");
        return rows;
    }

    async getTodo(id){
        const { rows } = await pool.query("select * from todos where id=$1;", [id]);
        return rows[0];
    }

    async addTodo(tarea) {
        await pool.query("insert into todos (todo) values($1);", [tarea])
    }

    async updateTodo(id, tarea) {
        await pool.query("update todos set todo = $1 where id = $2;", [tarea, id])
    }

    async deleteTodo(id) {
        await pool.query("delete from todos where id = $1;", [id]);
    }
}

// controlador
class Controller{
    constructor(model) {
        this.model = model;
    }

    async getTodos(req, res){
        const data = await this.model.getTodos();
        res.send(data);
    }

    async getTodo(req, res){
        const id = req.params.id;
        const data = await this.model.getTodo(id);
        res.send(data);
    }

    async addTodo(req, res) {
        const tarea = req.body.todo;
        await this.model.addTodo(tarea);
        res.sendStatus(201);
    }

    async updateTodo(req, res) {
        const id = req.params.id;
        const tarea = req.body.todo;
        await this.model.updateTodo(id, tarea)
        res.sendStatus(200);
    }
    
    async deleteTodo(req, res) {
        const id = req.params.id;
        await this.model.deleteTodo(id);
        res.sendStatus(204);
    }
}

const model = new Model();
const controller = new Controller(model);

app.use(express.json());

// creación de rutas
app.get("/todos", controller.getTodos.bind(controller));
app.get("/todos/:id", controller.getTodo.bind(controller));
app.post("/todos", controller.addTodo.bind(controller));
app.put("/todos/:id", controller.updateTodo.bind(controller));
app.delete("/todos/:id", controller.deleteTodo.bind(controller));



app.listen(port, () => {
    console.log(`Servidor levantado en http://localhost:${port}`)
});